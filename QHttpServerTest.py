from PyQt4.QtCore import QTimer, SIGNAL, QThread
from PyQt4.QtGui import QApplication
from PyQt4.QtNetwork import QHostAddress

from QHttpServer import QHttpServe
from QHttpServerModule import SayHello, Forbidden

import urllib2
import traceback

### HTTP Server Test Code
###
### Starts a server, then does a request to itself,
### (which has to be in a new thread)
### and then makes the app exit.
###

PASS="abcdef"

class ThreadGet(QThread):
    """
    This class defines a thread that gets a URL.
    """
    def __init__(self, url, app):
        QThread.__init__(self)
        self.url = url
        self.app = app
    def run(self):
        print "GET!!"
        try:
            req = urllib2.Request(self.url)
            req.add_header('Authentication', PASS)
            print urllib2.urlopen(req).read()
        except:
            print "Error..."
            print traceback.format_exc()
        print "GOT!"
        self.app.exit()

class Testget():
    """ 
    This class wraps the thread getter
    """
    def __init__(self, thread):
        self.thread = thread
    def get(self):
        self.thread.start()

####

if __name__ == "__main__":
    
    app = QApplication([], True)
    
    s = QHttpServe()
    s.setPassword(PASS)
    h = SayHello("Hello", "/fnord")
    s.registerModule(h)

    f = Forbidden("Forbidden")
    s.registerModule(f)

    s.listen(QHostAddress.LocalHost, 8090)

    threadget = ThreadGet("http://localhost:8090/fnord", app)
    testget = Testget(threadget)
    QTimer.singleShot(1*1000, testget.get)

    app.exec_()
