
class Module(object):
    """
    Module class definition.

    Need to figure out how to make proper abstract base classes really...
    """

    def __init__(self, label):
        self.label = label

    def registered_with(self, server):
        self.server = server

    def handles(self, request):
        raise ValueError,"method not implemented"

    def handle(self, request, connection):
        do_fun = "do_%s" % request.command
        if hasattr(self, do_fun):
            getattr(self, do_fun).__call__(request, connection)
        else:
            self.do_default(request)

    def do_default(self, request, connection):
        raise ValueError,"method not implemented"     
    def do_GET(self, request, connection):
        raise ValueError,"method not implemented"     
    def do_POST(self, request, connection):
        raise ValueError,"method not implemented"     
    def finish(self, connection):
        raise ValueError,"method not implemented"     

class SayHello(Module):
    """
    Simple test module

    Just echoes "Hello" to any request under a path
    """
    def __init__(self, label, root):
        Module.__init__(self, label)
        self.root = root

    def handles(self, request):
        if request.path.startswith(self.root):
            return True
        return False
    
    def do_GET(self, request, connection):

        print "Handled by SayHello"

        self.server.send_server_header()
        connection.write("Hello")
        connection.close()

    def do_POST(self, request):
        self.do_GET(request)
        print "POST payload:"
        print request.rfile.read()
        print "ENDS"

class Forbidden(Module):
    """ 
    returns a forbidden 403 code 
    """
    
    def handles(self, request):
        if request.path.startswith("/forbidden"):
            return True
        return False
    
    def do_GET(self, request, connection):
        self.server.send_forbidden()
