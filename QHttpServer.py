from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QApplication
from PyQt4.QtNetwork import QTcpServer, QHostAddress

from BaseHTTPServer import BaseHTTPRequestHandler
from StringIO import StringIO

from QHttpServerModule import SayHello


class HTTPRequest(BaseHTTPRequestHandler):
    def __init__(self, request_text):
        self.rfile = StringIO(request_text)
        self.raw_requestline = self.rfile.readline()
        self.error_code = self.error_message = None
        self.parse_request()

    def send_error(self, code, message):
        self.error_code = code
        self.error_message = message

class QHttpServe(QTcpServer):
    def __init__(self):
        QTcpServer.__init__(self)
        self.password = None
        self.modules = {}
        self.connect(self, SIGNAL("newConnection()"), self.slotNewConnection)

    def setPassword(self, password):
        """
        Set a password for the connection.
        Note this is transmitted in cleartext.
        """
        self.password = password

    def slotNewConnection(self):
        self.connection = self.nextPendingConnection()
        self.connect(self.connection, SIGNAL("readyRead()"), self.readData)

    def readData(self):
        request = HTTPRequest(self.connection.readAll())

        # if the server has a set password...
        if self.password:
            password = request.headers.getheader("Authentication")
            # fail if there's no authentication header
            if not password:
                self.send_forbidden()
                return
            # fail if the authentication password is wrong
            if password != self.password:
                self.send_forbidden()
                return
        # otherwise the password is correct.

        print "Server got request"
        print request.path
        for module in self.modules.values():
            if module.handles(request):
                module.handle(request, self.connection)
                return
        raise ValueError,"No module handler match found"
    
    def registerModule(self, module):
        module.registered_with(self)
        self.modules[module.label] = module

    def send_forbidden(self):
        self.connection.write("HTTP/1.0 403 FORBIDDEN\n")
        self.connection.write("Server: QServer\n")
        self.connection.close()

    def send_server_header(self):
        self.connection.write("HTTP/1.0 200 OK\n")
        self.connection.write("Server: QServer\n")
        self.connection.write("\n")

###
### As a test, just run the server, add a test module, and wait...
###

if __name__ == "__main__":
    
    app = QApplication([], True)
    
    s = QHttpServe()

    h = SayHello("Hello", "/")
    s.registerModule(h)

    s.listen(QHostAddress.LocalHost, 8090)

    app.exec_()
